#!/bin/bash

regions="AN VM VL MGN MD PuA LP IL VA Po LGN PuM Pul PuLL VP"

#thalamic regions
BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/Morel/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii'

for reg in $regions; do
flirt -in ${BASE}${reg}.nii.gz -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_mask.nii.gz
fslmaths ${BASE}${reg}_thr_mask.nii.gz -thr 0.5 -bin ${BASE}${reg}_thr_MNI_3mm.nii.gz
rm ${BASE}${reg}_thr_mask.nii.gz
done