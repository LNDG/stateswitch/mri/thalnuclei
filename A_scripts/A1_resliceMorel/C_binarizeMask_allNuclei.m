pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));
pn.MorelOut = [pn.root, 'analyses/I_thalamicNuclei/B_data/A_standards/Morel/'];

% load thalamic mask
morel = [pn.MorelOut, 'Thalamus_Morel_consolidated_mask_v3.nii'];

% load nifty with spatial components
morelData = load_untouch_nii(morel);

%% get binary matrices of subregions

% nuclei = {'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'Po'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'; 'VP'};
% nucleiID = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,17];

MorelMask = morelData.img;

MorelMask_c = zeros(numel(MorelMask),1);
MorelMask_c(MorelMask~=0) = 1;
MorelMask_c = reshape(MorelMask_c,size(MorelMask,1),...
    size(MorelMask,2), size(MorelMask,3));
morelData.img = MorelMask_c;
save_untouch_nii(morelData,[pn.MorelOut, 'AllNuclei.nii'])
