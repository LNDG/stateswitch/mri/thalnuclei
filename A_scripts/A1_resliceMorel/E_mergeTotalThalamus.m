% merge all thalamic nuclei

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B_PLS/T_tools/'];  addpath(genpath(pn.tools));
pn.MorelOut = [pn.root, 'analyses/I_thalamicNuclei/B_data/A_standards/Morel/'];
pn.thalamicOut = [pn.root, 'analyses/I_thalamicNuclei/B_data/B_thalamicTimeSeries/'];

%% load thalamic subdivisions

nuclei = {'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'Po'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'; 'VP'};

morelData = [];
for indComponent = 1:numel(nuclei)
    tmp = load_untouch_nii([pn.MorelOut, nuclei{indComponent}, '_thr_MNI_3mm.nii.gz']);
    morelData(indComponent,:) = tmp.img(:);
end

ThalamusComplete = mean(morelData,1);
ThalamusComplete(ThalamusComplete>0) = 1;

ThalamusComplete = reshape(ThalamusComplete, size(tmp.img));
tmp.img = ThalamusComplete;
save_untouch_nii(tmp, [pn.MorelOut, 'AllNuclei_3mm.nii'])