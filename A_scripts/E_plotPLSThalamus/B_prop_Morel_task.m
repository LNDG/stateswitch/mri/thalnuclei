% plot histogram of BSR voxels in Horn subregions
clear all

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/';
pn.dataBSR = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/';

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox'))
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/preprocessing_tools'))

% % get GM mask coordinates used in PLS
% maskpath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii';
% [mask] = double(S_load_nii_2d(maskpath));

% load BSR mask
BSR_img=S_load_nii_2d([pn.dataBSR, 'SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1.img'])';
BSR_img_unthresh = S_load_nii_2d([pn.dataBSR, 'SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded.img'])';

% region={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'Po'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'; 'VP'};
% regionNames={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'Po'; 'LGN'; 'PuM'; 'Pul'; 'PuLL'; 'VP'};

region={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuL'};
regionNames={'AN'; 'VM'; 'VL'; 'MGN'; 'MD'; 'PuA'; 'LP'; 'IL'; 'VA'; 'LGN'; 'PuM'; 'Pul'; 'PuL'};

% % create Horn mask for all subregions
% masks_cat=[];
% for r = 1:length(region)
%     p_part=[pn.data, 'A_standards/Morel/' region{r} '_thr_MNI_3mm.nii'];
%     img=S_load_nii_2d(p_part)';
%     masks_cat(r,:)=img; % save for concatenated mask
% end
% 
% MorelMask_all=mean(masks_cat,1);
% MorelMask_all(MorelMask_all~=0)=1; % necessary to account for potential overlap
% 
% % save total Horn Mask
% nii=load_nii(p_part);
% nii.img=[];
% 
% nii.img=MorelMask_all;
% save_nii(nii,[pn.data, 'C_BSRthal/MorelAll_Behav.nii.gz']);

%%  get overlap with individual thalamic projection zones

BSR_Morel=double(BSR_img_unthresh);

% get  BSR voxels for each part and compute percetange voxels
BSR_Morel_parts=[]; prop_Hornthal_parts = [];
for p2 = 1:length(region)
    p2_part=[pn.data, 'A_standards/Morel/' region{p2} '_thr_MNI_3mm.nii'];
    MorelMask=S_load_nii_2d(p2_part)';
    BSR_Morel_parts(p2,:)=nanmean(double(BSR_img_unthresh(logical(MorelMask))));
    prop_Hornthal_parts(p2,:)=numel(find(BSR_img(logical(MorelMask))~=0))/numel(find(MorelMask~=0));
end

% plot
h = figure('units','normalized','position',[.1 .1 .25 .1]);
set(gcf,'renderer','Painters')
cla; hold on;
[~, sortInd] = sort(prop_Hornthal_parts, 'descend');
scatter([1:numel(region)], prop_Hornthal_parts(sortInd)*100,100, 'k', 'filled')
ylabel({'% Voxels';'BSR >= 3'})
%[sortVal, sortInd] = sort(BSR_Morel_parts, 'ascend');
yyaxis right
scatter([1:numel(region)], BSR_Morel_parts(sortInd),100, 'r', 'filled') % invert for positive loading
xlabel('Thalamic nucleus (Morel)')
ylabel('mean BSR')
xlim([.5,length(region)+.5])
xticks([1:numel(regionNames)]); xticklabels(regionNames(sortInd))
set(findall(gcf,'-property','FontSize'),'FontSize',15)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/C_figures/E_ThalamusBSROverlap/';
figureName = 'TaskBSR_Morel';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
