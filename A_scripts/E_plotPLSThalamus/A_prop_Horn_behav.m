% plot histogram of BSR voxels in Horn subregions
clear all

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/';
pn.dataBSR = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/';

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox'))
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/preprocessing_tools'))

% % get GM mask coordinates used in PLS
% maskpath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii';
% [mask] = double(S_load_nii_2d(maskpath));

% load BSR mask
BSR_img=S_load_nii_2d([pn.dataBSR, 'SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1.img'])';
BSR_img_unthresh = S_load_nii_2d([pn.dataBSR, 'SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1_unthresholded.img'])';

region={'occipital' 'postparietal' 'prefrontal' 'premotor' 'primarymotor' 'sensory' 'temporal'};
regionNames={'Occipital' 'Parietal' 'Prefrontal' 'Premotor' 'Motor' 'Sensory' 'Temporal'};
% 
% % create Horn mask for all subregions
% masks_cat=[];
% for r = 1:length(region)
%     p_part=[pn.data, 'A_standards/Horn_2016_Thalamic_Connectivity_Atlas/mixed/' region{r} '_thr_MNI_3mm.nii'];
%     img=S_load_nii_2d(p_part)';
%     masks_cat(r,:)=img; % save for concatenated mask
% end
% 
% HornMask_all=mean(masks_cat,1);
% HornMask_all(HornMask_all~=0)=1; % necessary to account for potential overlap
% 
% % save total Horn Mask
% nii=load_nii(p_part);
% nii.img=[];
% 
% nii.img=HornMask_all;
% save_nii(nii,[pn.data, 'C_BSRthal/HornAll_Behav.nii.gz']);

%%  get overlap with individual thalamic projection zones

% get  BSR voxels for each part and compute percetange voxels
BSR_Hornthal_parts=[]; prop_Hornthal_parts = [];
for p2 = 1:length(region)
    p2_part=[pn.data, 'A_standards/Horn_2016_Thalamic_Connectivity_Atlas/mixed/' region{p2} '_thr_MNI_3mm.nii'];
    HornMask=S_load_nii_2d(p2_part)';
    % mask with Morel thalamic mask
%     MorelMask = S_load_nii_2d([pn.data, 'A_standards/Morel/AllNuclei_thr_MNI_3mm.nii']);
%     HornMask = (MorelMask'+HornMask)==2;
    BSR_Hornthal_parts(p2,:)=nanmean(double(BSR_img_unthresh(logical(HornMask))));
    prop_Hornthal_parts(p2,:)=numel(find(BSR_img(logical(HornMask))<-3))/numel(find(HornMask~=0));
end


% plot
h = figure('units','normalized','position',[.1 .1 .25 .1]);
set(gcf,'renderer','Painters')
cla; hold on;
%[sortVal, ~] = sort(prop_Hornthal_parts, 'descend');
[~, sortInd] = sort(BSR_Hornthal_parts, 'ascend');
scatter([1:numel(region)], prop_Hornthal_parts(sortInd)*100,100, 'k', 'filled')
ylabel({'% Voxels';'BSR >= 3'})
%[sortVal, sortInd] = sort(BSR_Hornthal_parts, 'ascend');
yyaxis right
scatter([1:numel(region)], -1.*BSR_Hornthal_parts(sortInd),100, 'r', 'filled') % invert for positive loading
xlabel('Thalamic projection zone (Horn area)')
ylabel('mean BSR')
xlim([.5,length(region)+.5])
xticklabels(regionNames(sortInd))
set(findall(gcf,'-property','FontSize'),'FontSize',15)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/C_figures/E_ThalamusBSROverlap/';
figureName = 'BehavBSR_Horn';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
