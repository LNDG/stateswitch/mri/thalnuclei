#!/bin/bash

# Scale intensity of atlas images to optimal range

# Determine robust range of atlas image within mask
# Rescale image to that range -- ie. map minimum to 0 and maximum to 4095

test -e /tmp/$USER || mkdir /tmp/$USER
td=$(mktemp -d /tmp/$USER/$(basename $0).XXXXXX) || exit 1
trap 'rm -rf $td' 0 1 15

img=$1 ; shift
msk=$1 ; shift
out=$1 ; shift

scale=1

fslmaths $img $td/image -odt float
imcp $msk $td/mask

cd $td
read min max <<< $(fslstats image -k mask -r) 
fslmaths image -thr $max -bin -mul $scale peaks 
fslmaths image -sub $min -thr 0 -mul $scale -div $(echo $max - $min | /usr/bin/bc ) -uthr $scale -add peaks out

cd -
imcp $td/out $out

