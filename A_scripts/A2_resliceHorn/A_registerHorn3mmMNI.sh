#!/bin/bash

regions="occipital postparietal prefrontal premotor primarymotor sensory temporal"

#thalamic regions
BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/Horn_2016_Thalamic_Connectivity_Atlas/mixed/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/A_standards/mni_icbm152_nlin_sym_09c/mni_icbm152_gm_tal_nlin_sym_09c_MNI_3mm.nii'
wbPath='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/T_tools/workbench/bin_macosx64'
affineMat='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/T_tools/affine.txt' # needed for wb_command

for reg in $regions; do
	# rescale absolute probabilities to lie between 0 and 1, threshold at 50% of this RELATIVE probability
	./scalegeneric.sh ${BASE}${reg}.nii.gz ${BASE}${reg}.nii.gz ${BASE}${reg}_thr_mask.nii.gz
	fslmaths ${BASE}${reg}_thr_mask.nii.gz -thr .75 -bin ${BASE}${reg}_thr_mask.nii.gz
	${wbPath}/wb_command -volume-affine-resample ${BASE}${reg}_thr_mask.nii.gz ${affineMat} ${REF} ENCLOSING_VOXEL ${BASE}${reg}_thr_MNI_3mm.nii.gz
	#flirt -in ${BASE}${reg}_thr_mask.nii.gz -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_MNI_3mm.nii.gz
	#fslmaths ${BASE}${reg}_thr_MNI_3mm.nii.gz -thr 0 -bin ${BASE}${reg}_thr_mask.nii.gz
	rm ${BASE}${reg}_thr_mask.nii.gz
done